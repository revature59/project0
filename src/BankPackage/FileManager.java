package BankPackage;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;





public class FileManager implements Serializable {
	
	//reader writer streams
	private BufferedReader br;
	private BufferedWriter bw;
	
	//Directory paths
	private String filePath="files/";
	private String fileCustomers="customerList"; 
	private String fileUsers="userRegistry";
	private String fileAdmins="adminList";
	private String fileEmployee="employeeList";
	private String fileTransactions="transactionList";
	private String fileAccounts="aouccntList";
	private Map<String,Customer> customers=new HashMap<>();
	private Map<String,User> users=new HashMap<>();
	private Map<String,Admin> admins=new HashMap<>();
	private Map<String,Employee> employees=new HashMap<>();
	private Map<String,Transaction> transactions=new HashMap<>();
	private Map<String,Account> accounts=new HashMap<>();
	
	
	
	String c;
	//public Map setUsers();
	
	public String filePath()
	{
		return this.filePath;
	}
	
	//path methods
	public String getFileUsers()
	{
		return this.fileUsers;
	}
	
	public String getFileCustomers()
	{
		return this.fileCustomers;
	}
	
	
	
	//hashmap methods
	
	public Map<String,User> getUsers() {
		return this.users;
	}
	
	public void setUsers(Map<String,User> users)
	{
		this.users=users;
	}
	
	public Map<String,Customer> getCustomers() {
		return this.customers;
	}
	
	public void setCustomers(Map<String,Customer> customers)
	{
		this.customers=customers;
	}
	
	//filemanager methods
	
	public void readFromFile(String fileName) throws IOException
	{
		//unSerializeRecords(filePath()+fileName));
		
		
		try
		{
			br=new BufferedReader(new FileReader(filePath()+fileName));
		
			System.out.println("About to read from a file");
		
			while((c=br.readLine()) != null)
			{
				System.out.println(c);
			}
			br.close();
			System.out.println("\nFinished reading fromfile");
		}catch(IOException e)
		{
			e.printStackTrace();
		}

	}
	
	public void writeToFile(String userInput, String fileName) throws IOException
	{
		try
		{
		bw=new BufferedWriter(new FileWriter(filePath()+fileName));
		
		String writeMe= userInput;
		
		System.out.println("About to write to a file");
		bw.write(writeMe);
		bw.newLine();
		
		
	
		bw.close();
		System.out.println("\nFinished writing to file");
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	//serializing
	public void serializeRecords(String file) throws IOException
	{
		try
		{
			FileOutputStream fout=new FileOutputStream(filePath+ file);//saving state of an object
		
			ObjectOutputStream out=new ObjectOutputStream(fout);
		}catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}
	//Object
	@SuppressWarnings("unchecked")
	public Map unSerializeRecords(String file)  throws IOException, ClassNotFoundException
	{
		
		try
		{
			ObjectInputStream in= new ObjectInputStream(new FileInputStream(filePath+file));
			switch(file)
			{
			case "customerList":
				this.customers=(Map<String,Customer>)in.readObject();
				in.close();
				return this.customers;
			case "userRegistry":
				this.users =(Map<String,User>)in.readObject();
				in.close();
				return this.users;
			case "adminList":
				this.admins=(Map<String,Admin>)in.readObject();
				in.close();
				return this.admins;
			case "employeeList":
				this.employees=(Map<String,Employee>)in.readObject();
				in.close();
				return this.employees;
			case "transactionList":
	         	this.transactions=(Map<String,Transaction>)in.readObject();
				in.close();
				return this.transactions;
			case "aouccntList":
				this.accounts=(Map<String,Account>)in.readObject();
				in.close();
				return this.accounts;
						
				
			}
			in.close();		
		}catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}catch(IOException e)
		{
			e.printStackTrace();
		}
	
		return null;
	}
	
	

}

