package BankPackage;

import java.io.Serializable;

public class User implements Serializable {
	String ID;
	String firstname;
	String lastname;
	String username;
	String password;
	String email;
	String status;
	String type;
	
	User(String ID,	String firstname, String lastname, 	String username, String password, String email, String status, String type)
	{
		setID(ID);
		this.firstname=firstname;
		this.lastname = lastname;
		setUsername(username);
		setPassword(password);
		setEmail(email);
		setStatus(status);
		setType(type);
	}
	
	//to string was to complex so we had to override usually json files are enough to use regular toString()
	@Override
	public String toString() {
		
		return ID+firstname+ lastname+ username +password +email+status+type;
		
	}
	
	//get methods
	public String getID()
	{
		return ID;
	}
	
	public String getName()
	{
		return firstname+" "+lastname;
	}
		
	public String getUsername()
	{
		return username;
	}
	
	public String getPassword()
	{
		return password;
	}
	public String getEmail()
	{
		return email;
	}
	public String getStatus()
	{
		return status;
	}
	public String getType()
	{
		return type;
	}
	
	
	//set methods
	public void setID(String ID) {
		this.ID=ID;
	}
	public void setName(String firstname, String lastname) {
		this.firstname=firstname;
		this.lastname=lastname;
	}
	
	public void setPassword(String password) {
		this.password=password;
	}
	public void setUsername(String username) {
		this.username=username;
		
	}
	
	public void setEmail(String password) {
		this.password=password;
	}
	public void setStatus(String status) {
		this.status=status;
		
	}
	
	public void setType(String type) {
		this.type=type;
	}
	
	
	
	public void register(String firstname, String lastname, String username, String password) {
		setType("Customer");
	
		//call display
		//call write
		//check for duplicates
	}
	
	
	public void logsin(String inUsername, String inPassword) {
		//verification
		//
	}
	
	public void packageRecord()
	{
		
	}
	public void unpackageRecord()
	{
		
	}
	
}





