package BankPackage;

public class Account {

	private int balance;
	private String number;
	private String status;
	private String owner;
	Account(String number,int balance,String status, String owner)
	{
		this.number=number;
		this.balance=balance;
		this.status=status;
		this.owner=owner;
	}
	
	
	
	public String getAccountNumber()
	{
		return this.number;
	}
	
	public int getAccountBalance()
	{
		return this.balance;
	}
	
	public String getAccountStatus()
	{
		return this.status;
	}
	
	public String getOwner()
	{
		return this.owner;
	}
	
	
	public void setAccountNumber(String number)
	{
		this.number=number;
	}
	
	public void setAccountBalance(int balance)
	{
		this.balance=balance;
	}
	
	public void setAccountStatus(String status)
	{
		this.status=status;
	}
	public void getAccountnumber(String owner)
	{
		this.owner=owner;
	}
}
